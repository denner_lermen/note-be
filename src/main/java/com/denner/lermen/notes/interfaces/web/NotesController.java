package com.denner.lermen.notes.interfaces.web;


import com.denner.lermen.notes.domain.notes.model.Note;
import com.denner.lermen.notes.domain.notes.model.NoteRepository;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

@RestController
@RequestScope
public class NotesController {

  private NoteRepository noteRepository;

  public NotesController(NoteRepository noteRepository) {
    this.noteRepository = noteRepository;
  }

  @PostMapping(path = "/v1/notes")
  public Note create(@RequestBody Note note) {
    return noteRepository.save(note);
  }

  @GetMapping(path = "/v1/notes/{id}")
  public ResponseEntity<Note> findOne(@PathVariable("id") String id) {
    Optional<Note> noteOptional = noteRepository.findById(id);
    return ResponseEntity.of(noteOptional);
  }

  @PutMapping(path = "/v1/notes/{id}")
  public ResponseEntity<Note> findOne(@PathVariable("id") String id, @RequestBody Note note) {
    Optional<Note> noteOptional = noteRepository.findById(id);

    if (noteOptional.isPresent()) {
      Note noteToUpdate = noteOptional.get();
      noteToUpdate.setNote(note.getNote());
      noteToUpdate.setTitle(note.getTitle());
      return ResponseEntity.ok(noteRepository.save(noteToUpdate));
    }

    return ResponseEntity.notFound().build();
  }

  @GetMapping(path = "/v1/notes")
  public Page<Note> findAll(@PageableDefault(size = 100) @SortDefault(sort = "createdAt", direction = Sort.Direction.DESC) Pageable pageable) {
    return noteRepository.findAll(pageable);
  }

}
